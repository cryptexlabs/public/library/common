export class Constants {
    private constructor() {}
    public static readonly PRICE_DECIMAL_PLACES: number = 8;
    public static readonly QUANTITY_DECIMAL_PLACES: number = 8;
}