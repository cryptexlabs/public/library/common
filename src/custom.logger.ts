import {Injectable, Logger} from "@nestjs/common";

@Injectable()
export class CustomLogger extends Logger {

    constructor(private defaultContext: string = "info", private readonly filterContext?: string[], isTimeDiffEnabled?: boolean) {
        super(defaultContext, isTimeDiffEnabled);
    }

    public log(message: string, context: string = "") {
        if (context === "") {
            context = this.defaultContext;
        }
        if (this.filterContext && this.filterContext.length) {
            if (this.filterContext.indexOf(context) !== -1) {
                super.log(message, context);
            }
        } else {
            super.log(message, context);
        }
    }

    public error(message: string, trace: string, context: string = "") {
        if (context === "") {
            super.error(message, "", "error");
        } else {
            super.error(message, context);
        }
    }
}