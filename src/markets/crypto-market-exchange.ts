import {MarketExchangeConfigInterface} from "./market-exchange-config.interface";

export class CryptoMarketExchange {
    public static readonly GDAX: MarketExchangeConfigInterface = {
        id: "GDAX",
    };
}