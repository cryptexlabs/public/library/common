import {MarketExchangeConfigInterface} from "./market-exchange-config.interface";

export class StockMarketExchange {
    public static readonly TFF: MarketExchangeConfigInterface = {
        id: "TFF",
    };
    public static readonly XASE: MarketExchangeConfigInterface = {
        id: "XASE",
    };
    public static readonly XNAS: MarketExchangeConfigInterface = {
        id: "XNAS",
    };
    public static readonly XCIS: MarketExchangeConfigInterface = {
        id: "XCIS",
    };
    public static readonly FINR: MarketExchangeConfigInterface = {
        id: "FINR",
    };
    public static readonly CQS: MarketExchangeConfigInterface = {
        id: "CQS",
    };
    public static readonly XISX: MarketExchangeConfigInterface = {
        id: "XISX",
    };
    public static readonly EDGA: MarketExchangeConfigInterface = {
        id: "EDGA",
    };
    public static readonly EDGX: MarketExchangeConfigInterface = {
        id: "EDGX",
    };
    public static readonly XCHI: MarketExchangeConfigInterface = {
        id: "XCHI",
    };
    public static readonly XNYS: MarketExchangeConfigInterface = {
        id: "XNYS",
    };
    public static readonly ARCX: MarketExchangeConfigInterface = {
        id: "ARCX",
    };
    public static readonly XNGS: MarketExchangeConfigInterface = {
        id: "XNGS",
    };
    public static readonly CTS: MarketExchangeConfigInterface = {
        id: "CTS",
    };
    public static readonly OOTC: MarketExchangeConfigInterface = {
        id: "OOTC",
    };
    public static readonly XOTC: MarketExchangeConfigInterface = {
        id: "XOTC",
    };
    public static readonly PSGM: MarketExchangeConfigInterface = {
        id: "PSGM",
    };
    public static readonly PINX: MarketExchangeConfigInterface = {
        id: "PINX",
    };
    public static readonly OTCB: MarketExchangeConfigInterface = {
        id: "OTCB",
    };
    public static readonly OTCQ: MarketExchangeConfigInterface = {
        id: "OTCQ",
    };
    public static readonly IEXG: MarketExchangeConfigInterface = {
        id: "IEXG",
    };
    public static readonly XCBO: MarketExchangeConfigInterface = {
        id: "XCBO",
    };
    public static readonly PHLX: MarketExchangeConfigInterface = {
        id: "PHLX",
    };
    public static readonly BATY: MarketExchangeConfigInterface = {
        id: "BATY",
    };
    public static readonly BATS: MarketExchangeConfigInterface = {
        id: "BATS",
    };
    public static readonly XBOS: MarketExchangeConfigInterface = {
        id: "XBOS",
    };
    public static readonly SPIC: MarketExchangeConfigInterface = {
        id: "SPIC",
    };
    public static readonly SPIB: MarketExchangeConfigInterface = {
        id: "SPIB",
    };
    public static readonly RUS: MarketExchangeConfigInterface = {
        id: "RUS",
    };
    public static readonly MDX: MarketExchangeConfigInterface = {
        id: "MDX",
    };
    public static readonly DJI: MarketExchangeConfigInterface = {
        id: "DJI",
    };
}