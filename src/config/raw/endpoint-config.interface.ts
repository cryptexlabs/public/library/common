export interface EndpointConfigRawInterface {
    host: string;
    port: number;
}