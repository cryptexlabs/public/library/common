import {EndpointConfigInterface} from "./endpoint-config.interface";

export interface ApiConfigInterface {
    getEndpoints(): EndpointConfigInterface[];
}