export interface EndpointConfigInterface {
    getHost(): string;
    getPort(): number;
}