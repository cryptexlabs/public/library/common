export declare class Constants {
    private constructor();
    static readonly PRICE_DECIMAL_PLACES: number;
    static readonly QUANTITY_DECIMAL_PLACES: number;
}
