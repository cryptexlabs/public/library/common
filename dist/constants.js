"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Constants {
    constructor() { }
}
Constants.PRICE_DECIMAL_PLACES = 8;
Constants.QUANTITY_DECIMAL_PLACES = 8;
exports.Constants = Constants;
//# sourceMappingURL=constants.js.map