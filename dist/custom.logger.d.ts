import { Logger } from "@nestjs/common";
export declare class CustomLogger extends Logger {
    private defaultContext;
    private readonly filterContext?;
    constructor(defaultContext?: string, filterContext?: string[], isTimeDiffEnabled?: boolean);
    log(message: string, context?: string): void;
    error(message: string, trace: string, context?: string): void;
}
