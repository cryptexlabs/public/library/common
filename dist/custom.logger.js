"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
let CustomLogger = class CustomLogger extends common_1.Logger {
    constructor(defaultContext = "info", filterContext, isTimeDiffEnabled) {
        super(defaultContext, isTimeDiffEnabled);
        this.defaultContext = defaultContext;
        this.filterContext = filterContext;
    }
    log(message, context = "") {
        if (context === "") {
            context = this.defaultContext;
        }
        if (this.filterContext && this.filterContext.length) {
            if (this.filterContext.indexOf(context) !== -1) {
                super.log(message, context);
            }
        }
        else {
            super.log(message, context);
        }
    }
    error(message, trace, context = "") {
        if (context === "") {
            super.error(message, "", "error");
        }
        else {
            super.error(message, context);
        }
    }
};
CustomLogger = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [String, Array, Boolean])
], CustomLogger);
exports.CustomLogger = CustomLogger;
//# sourceMappingURL=custom.logger.js.map