import { ExchangeConfigInterface } from "./exchange-config.interface";
export interface ExchangesConfigInterface {
    getExchanges(): ExchangeConfigInterface[];
}
