import { EndpointConfigRawInterface } from "./endpoint-config.interface";
export interface ApiConfigRawInterface {
    endpoints: EndpointConfigRawInterface[];
    apiKey: string;
}
