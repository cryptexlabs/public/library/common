import { ExchangeConfigRawInterface } from "./exchange-config-raw.interface";
export interface ExchangesConfigRawInterface {
    exchanges: string[] | ExchangeConfigRawInterface[];
}
