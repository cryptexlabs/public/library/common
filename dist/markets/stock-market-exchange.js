"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class StockMarketExchange {
}
StockMarketExchange.TFF = {
    id: "TFF",
};
StockMarketExchange.XASE = {
    id: "XASE",
};
StockMarketExchange.XNAS = {
    id: "XNAS",
};
StockMarketExchange.XCIS = {
    id: "XCIS",
};
StockMarketExchange.FINR = {
    id: "FINR",
};
StockMarketExchange.CQS = {
    id: "CQS",
};
StockMarketExchange.XISX = {
    id: "XISX",
};
StockMarketExchange.EDGA = {
    id: "EDGA",
};
StockMarketExchange.EDGX = {
    id: "EDGX",
};
StockMarketExchange.XCHI = {
    id: "XCHI",
};
StockMarketExchange.XNYS = {
    id: "XNYS",
};
StockMarketExchange.ARCX = {
    id: "ARCX",
};
StockMarketExchange.XNGS = {
    id: "XNGS",
};
StockMarketExchange.CTS = {
    id: "CTS",
};
StockMarketExchange.OOTC = {
    id: "OOTC",
};
StockMarketExchange.XOTC = {
    id: "XOTC",
};
StockMarketExchange.PSGM = {
    id: "PSGM",
};
StockMarketExchange.PINX = {
    id: "PINX",
};
StockMarketExchange.OTCB = {
    id: "OTCB",
};
StockMarketExchange.OTCQ = {
    id: "OTCQ",
};
StockMarketExchange.IEXG = {
    id: "IEXG",
};
StockMarketExchange.XCBO = {
    id: "XCBO",
};
StockMarketExchange.PHLX = {
    id: "PHLX",
};
StockMarketExchange.BATY = {
    id: "BATY",
};
StockMarketExchange.BATS = {
    id: "BATS",
};
StockMarketExchange.XBOS = {
    id: "XBOS",
};
StockMarketExchange.SPIC = {
    id: "SPIC",
};
StockMarketExchange.SPIB = {
    id: "SPIB",
};
StockMarketExchange.RUS = {
    id: "RUS",
};
StockMarketExchange.MDX = {
    id: "MDX",
};
StockMarketExchange.DJI = {
    id: "DJI",
};
exports.StockMarketExchange = StockMarketExchange;
//# sourceMappingURL=stock-market-exchange.js.map